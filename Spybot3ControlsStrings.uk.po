# Patrick Kolla <patrick.kolla@safer-networking.org>, 2024.
# DeepL <noreply-mt-deepl@weblate.org>, 2024.
msgid ""
msgstr ""
"PO-Revision-Date: 2024-01-12 19:10+0000\n"
"Last-Translator: Patrick Kolla <patrick.kolla@safer-networking.org>\n"
"Language-Team: Ukrainian <http://translations.spybot.de/projects/"
"spybot-anti-beacon/ui-controls/uk/>\n"
"Language: uk\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Weblate 5.3\n"

#: spybot3controlsstrings.rscheckboxoff
msgid "off"
msgstr "вимкнено"

#: spybot3controlsstrings.rscheckboxon
msgid "on"
msgstr "на"

#: spybot3controlsstrings.rsdrivetypecdrom
msgid "CDROM"
msgstr "CD-ROM"

#: spybot3controlsstrings.rsdrivetypefixed
msgid "Fixed"
msgstr "Виправлено"

#: spybot3controlsstrings.rsdrivetypememorydisk
msgid "Memory Disk"
msgstr "Диск пам'яті"

#: spybot3controlsstrings.rsdrivetyperemote
msgid "Remote"
msgstr "Пульт дистанційного керування"

#: spybot3controlsstrings.rsdrivetyperemovable
msgid "Removable"
msgstr "Знімний"

#: spybot3controlsstrings.rsdrivetypeunknown
msgid "Unknown"
msgstr "Невідомо"

#: spybot3controlsstrings.rsstatisticshistoryallthreads
msgid "All threats"
msgstr "Всі загрози"

#: spybot3controlsstrings.rsthreatscountspecifier
msgid "Threats"
msgstr "Загрози"

#: spybot3controlsstrings.rswheelsecuritylevel
msgid "Security Level"
msgstr "Рівень безпеки"
